table! {
    assignment (id) {
        id -> Int4,
        name -> Nullable<Varchar>,
        language -> Nullable<Varchar>,
        testfile -> Nullable<Varchar>,
    }
}
