// #![feature(plugin, custom_derive)]
// #![plugin(rocket_codegen)]
#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

extern crate rocket_contrib;
extern crate tera;

use rocket_contrib::templates::Template;
use tera::Context;


#[get("/")]
fn index() -> Template {
    let mut context = Context::new();

    context.add("title", &"Test Grader");
    Template::render("base", &context)
}

fn main() {
    rocket::ignite()
           .mount("/", routes![index])
           .attach(Template::fairing())
           .launch();
   }
